# Generacions del llenguatges de programació

![foto](https://lh3.googleusercontent.com/-gPclgNhF0tY/TXhz96TzfiI/AAAAAAAAABY/Whjo3TjBFsk/s1600/ESTRUCTURA.bmp)
___
## **En què es caracteritzen?**

Són llenguatges  molt més independents de la màquina i més programables.
Inclouen funcions com la compatibilitat millorada dels tipus de dades agregades i l'expressió de conceptes de manera que afavoreixi el programador i no l'ordinador. 
Un llenguatge de tercera generació millora el llenguatge de segona generació, ja que l'equip s'encarrega de detalls no essencials. 

## Cap a quins sectors estan destinats?

Aquests llenguatges estan destinats a especialistes, programadors i ambients en què calgui desenvolupar programes i sistemes que requereixen un procediment específic per a l'ordinador.


## **Quins llenguatges hi podem trobar?**

Els primers llenguatges de tercera generació es van veure per primera vegada a finals de la dècada de 1950.
**Alguns exemples son: Fortran, ALGOL i COBOL**

En la actualitat podem trobar llenguatges com:**C, Fortran, Smalltalk, Ada, C ++, C #, Cobol, Delphi, Java, PHP, etc.**

# Fortran

```fortran
C AREA OF A TRIANGLE - HERON'S FORMULA
C INPUT - CARD READER UNIT 5, INTEGER INPUT
C OUTPUT -
C INTEGER VARIABLES START WITH I,J,K,L,M OR N
      READ(5,501) IA,IB,IC
  501 FORMAT(3I5)
      IF(IA.EQ.0 .OR. IB.EQ.0 .OR. IC.EQ.0) STOP 1
      S = (IA + IB + IC) / 2.0
      AREA = SQRT( S * (S - IA) * (S - IB) * (S - IC) )
      WRITE(6,601) IA,IB,IC,AREA
  601 FORMAT(4H A= ,I5,5H  B= ,I5,5H  C= ,I5,8H  AREA= ,F10.2,
     $13H SQUARE UNITS)
      STOP
      END
```

# COBOL

```cobol
000010 IDENTIFICATION DIVISION.
000020 PROGRAM-ID.       SAMPLE.
000030 AUTHOR.           J.P.E. HODGSON.
000040 DATE-WRITTEN.     4 February 2000
000041
000042* A sample program just to show the form.
000043* The program copies its input to the output,
000044* and counts the number of records.
000045* At the end this number is printed.
000046
000050 ENVIRONMENT DIVISION.
000060 INPUT-OUTPUT SECTION.
000070 FILE-CONTROL.
000080     SELECT STUDENT-FILE     ASSIGN TO SYSIN
000090         ORGANIZATION IS LINE SEQUENTIAL.
000100     SELECT PRINT-FILE       ASSIGN TO SYSOUT
000110         ORGANIZATION IS LINE SEQUENTIAL.
000120
000130 DATA DIVISION.
000140 FILE SECTION.
000150 FD  STUDENT-FILE
000160     RECORD CONTAINS 43 CHARACTERS
000170     DATA RECORD IS STUDENT-IN.
000180 01  STUDENT-IN              PIC X(43).
000190
000200 FD  PRINT-FILE
000210     RECORD CONTAINS 80 CHARACTERS
000220     DATA RECORD IS PRINT-LINE.
000230 01  PRINT-LINE              PIC X(80).
000240
000250 WORKING-STORAGE SECTION.
000260 01  DATA-REMAINS-SWITCH     PIC X(2)      VALUE SPACES.
000261 01  RECORDS-WRITTEN         PIC 99.
000270
000280 01  DETAIL-LINE.
000290     05  FILLER              PIC X(7)      VALUE SPACES.
000300     05  RECORD-IMAGE        PIC X(43).
000310     05  FILLER              PIC X(30)     VALUE SPACES.
000311 
000312 01  SUMMARY-LINE.
000313     05  FILLER              PIC X(7)      VALUE SPACES.
000314     05  TOTAL-READ          PIC 99.
000315     05  FILLER              PIC X         VALUE SPACE.
000316     05  FILLER              PIC X(17)     
000317                 VALUE  'Records were read'.
000318     05  FILLER              PIC X(53)     VALUE SPACES.
000319
000320 PROCEDURE DIVISION.
000321
000330 PREPARE-SENIOR-REPORT.
000340     OPEN INPUT  STUDENT-FILE
000350          OUTPUT PRINT-FILE.
000351     MOVE ZERO TO RECORDS-WRITTEN.
000360     READ STUDENT-FILE
000370         AT END MOVE 'NO' TO DATA-REMAINS-SWITCH
000380     END-READ.
000390     PERFORM PROCESS-RECORDS
000410         UNTIL DATA-REMAINS-SWITCH = 'NO'.
000411     PERFORM PRINT-SUMMARY.
000420     CLOSE STUDENT-FILE
000430           PRINT-FILE.
000440     STOP RUN.
000450
000460 PROCESS-RECORDS.
000470     MOVE STUDENT-IN TO RECORD-IMAGE.
000480     MOVE DETAIL-LINE TO PRINT-LINE.
000490     WRITE PRINT-LINE.
000500     ADD 1 TO RECORDS-WRITTEN.
000510     READ STUDENT-FILE
000520         AT END MOVE 'NO' TO DATA-REMAINS-SWITCH
000530     END-READ. 
000540
000550 PRINT-SUMMARY.
000560     MOVE RECORDS-WRITTEN TO TOTAL-READ.
000570     MOVE SUMMARY-LINE TO PRINT-LINE.
000571     WRITE PRINT-LINE. 
000572
000580       
       
```


[Quarta_Generació](https://gitlab.com/53335394g/primera_generacion/blob/master/cuarta_generaci%C3%B3n.md)



